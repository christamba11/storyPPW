from django.shortcuts import render

def homeView(request):
    return render(request,'story3/homepage.html')

def projectView(request):
    return render(request, 'story3/my-projects.html')