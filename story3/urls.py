from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from . import views

app_name="story3"

urlpatterns = [
    path('', views.homeView, name="homepage"),
    path('projects/', views.projectView, name="projects")
]
